//config inicial
require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const app = express()

//forma de ler JSON / middlewares
app.use(
  express.urlencoded({
    extended: true
  })
)

app.use(express.json())

//rotas Api
const personRoutes = require('./routes/personRoutes')
app.use('/person', personRoutes)

//Rota inicial / endpoint
app.get('/', (req, res) => {
  //mostrar requisição
  res.json({
    message: 'Api home works',
    date: '02/07/2022',
    author: 'LuizF3lipe dev'
  })
})

//entregar uma porta / e conexão banco
const DB_USER = process.env.DB_USER
const DB_PASSWORD = encodeURIComponent(process.env.DB_PASSWORD)

mongoose.connect(
  `mongodb+srv://${DB_USER}:${DB_PASSWORD}@apicluster.woa19.mongodb.net/bancoapi?retryWrites=true&w=majority`
)
.then(() => {
  console.log('Conectamos ao MongoDB!')
  app.listen(3000)
})
.catch((err) => console.log(err))